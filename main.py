from flask import Flask, render_template

app = Flask(__name__)


@app.route('/expedicao')
def index():
    return render_template("index.html")

@app.route('/expedicao/cronograma')
def cronograma():
    return render_template("cronograma.html")

@app.route('/expedicao/informacoes')
def informacoes():
    return render_template("informacoes.html")

@app.route('/expedicao/apoiadores')
def apoiadores():
    return render_template("apoiadores.html")


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=8000)
